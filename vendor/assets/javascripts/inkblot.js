(function() {
  var Inkblot, Particle, ParticleEmitter, color_string_from_rgba_object, deg_to_rad, emitter_config, rad_to_deg, shallow_copy, signed_random, wrap_angle;

  Particle = (function() {
    function Particle(pos, ctx, cfg) {
      this.ctx = ctx;
      this.pos = pos;
      this.dodge_burn = Math.random() * 2;
      this.cfg = cfg;
    }

    Particle.prototype.step = function(dt) {
      if (this.dead) {
        return;
      }
      this.cfg.LIFE -= dt;
      if (this.cfg.LIFE <= 0) {
        this.dead = true;
      }
      this.pos = {
        x: Math.min(Math.max(this.pos.x + (this.cfg.VELOCITY.X * dt), 0 + this.cfg.SIZE), this.ctx.canvas.width - this.cfg.SIZE),
        y: Math.min(Math.max(this.pos.y + (this.cfg.VELOCITY.Y * dt), 0 + this.cfg.SIZE), this.ctx.canvas.height - this.cfg.SIZE)
      };
      this.cfg.VELOCITY = {
        X: this.cfg.VELOCITY.X * this.cfg.SPEED_DECAY.X,
        Y: this.cfg.VELOCITY.Y * this.cfg.SPEED_DECAY.Y
      };
      return this.draw();
    };

    Particle.prototype.draw = function() {
      var dodge_burned, dodge_burned_trans, half_size, rad_grad, x, y;
      half_size = this.cfg.SIZE >> 1;
      x = ~~(this.pos.x - half_size);
      y = ~~(this.pos.y - half_size);
      rad_grad = this.ctx.createRadialGradient(x + half_size, y + half_size, this.cfg.RADIAL_INNER, x + half_size, y + half_size, half_size);
      if (Math.random() <= this.cfg.DODGE_BURN_CHANCE) {
        this.dodge_burn *= Math.random() * 2;
        dodge_burned = {
          r: Math.min(Math.floor(this.cfg.COLOR.r * this.dodge_burn), 255),
          g: Math.min(Math.floor(this.cfg.COLOR.g * this.dodge_burn), 255),
          b: Math.min(Math.floor(this.cfg.COLOR.b * this.dodge_burn), 255),
          a: this.cfg.COLOR.a
        };
        dodge_burned_trans = {
          r: dodge_burned.r,
          g: dodge_burned.g,
          b: dodge_burned.b,
          a: 0
        };
        rad_grad.addColorStop(0, color_string_from_rgba_object(dodge_burned));
        rad_grad.addColorStop(1, color_string_from_rgba_object(dodge_burned_trans));
      } else {
        rad_grad.addColorStop(0, color_string_from_rgba_object(this.cfg.COLOR));
        rad_grad.addColorStop(1, color_string_from_rgba_object(this.cfg.COLOR_TRANS));
      }
      this.ctx.save();
      if (Math.random() <= this.cfg.COMPOSITE_LIGHTER_CHANCE) {
        this.ctx.globalCompositeOperation = 'lighter';
      } else {
        this.ctx.globalCompositeOperation = 'darker';
      }
      this.ctx.beginPath();
      this.ctx.fillStyle = rad_grad;
      this.ctx.arc(this.pos.x, this.pos.y, half_size, Math.PI * 2, false);
      this.ctx.fill();
      return this.ctx.restore();
    };

    return Particle;

  })();

  ParticleEmitter = (function() {
    function ParticleEmitter(pos, rng, ctx, cfg) {
      this.pos = pos;
      this.rng = rng;
      this.ctx = ctx;
      this.particles = [];
      this.some_alive = true;
      this.cfg = cfg;
    }

    ParticleEmitter.prototype.step = function(dt) {
      var p, _i, _len, _ref, _results;
      this.some_alive = void 0;
      this.pos = {
        x: this.pos.x + dt * this.cfg.VELOCITY.X * this.cfg.REFL.X,
        y: this.pos.y + dt * this.cfg.VELOCITY.Y * this.cfg.REFL.Y
      };
      if (this.cfg.NUM_PARTICLES > 0) {
        this.fire();
      }
      _ref = this.particles;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        p = _ref[_i];
        if (!p.dead) {
          p.step(dt);
        }
        if (!p.dead) {
          _results.push(this.some_alive != null ? this.some_alive : this.some_alive = true);
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };

    ParticleEmitter.prototype.fire = function() {
      var offset_pos, p, particle_cfg;
      particle_cfg = shallow_copy(this.cfg.PARTICLES);
      particle_cfg.SIZE += particle_cfg.SIZE_VARIANCE * signed_random(this.rng);
      if (particle_cfg.SIZE === 0) {
        particle_cfg.SIZE = 0;
      }
      particle_cfg.LIFE += particle_cfg.LIFE_VARIANCE * signed_random(this.rng);
      offset_pos = {
        x: this.pos.x + signed_random(this.rng) * this.cfg.POS_VARIANCE.X,
        y: this.pos.y + signed_random(this.rng) * this.cfg.POS_VARIANCE.Y
      };
      particle_cfg.EMIT_ANGLE = deg_to_rad(this.cfg.EMIT_ANGLE + this.cfg.EMIT_ANGLE_VARIANCE * signed_random(this.rng));
      particle_cfg.EMIT_ANGLE = wrap_angle(particle_cfg.EMIT_ANGLE);
      particle_cfg.SPEED += particle_cfg.SPEED_VARIANCE * signed_random(this.rng);
      particle_cfg.VELOCITY = {
        X: Math.cos(particle_cfg.EMIT_ANGLE) * particle_cfg.SPEED * this.cfg.REFL.X,
        Y: Math.sin(particle_cfg.EMIT_ANGLE) * particle_cfg.SPEED * this.cfg.REFL.Y
      };
      particle_cfg.RADIAL_SHARPNESS += particle_cfg.RADIAL_SHARPNESS_VARIANCE * signed_random(this.rng);
      if (particle_cfg.RADIAL_SHARPNESS > 100) {
        particle_cfg.RADIAL_SHARPNESS = 100;
      }
      if (particle_cfg.RADIAL_SHARPNESS < 0) {
        particle_cfg.RADIAL_SHARPNESS = 0;
      }
      particle_cfg.RADIAL_INNER = particle_cfg.SIZE / 200 * particle_cfg.RADIAL_SHARPNESS;
      p = new Particle(offset_pos, this.ctx, particle_cfg);
      this.particles.push(p);
      return this.cfg.NUM_PARTICLES -= 1;
    };

    return ParticleEmitter;

  })();

  emitter_config = {
    NUM_PARTICLES: 50,
    ROTATION_SPEED: 0,
    EMIT_ANGLE: 0,
    EMIT_ANGLE_VARIANCE: 180,
    VELOCITY: {
      X: 0.1,
      Y: 0.1
    },
    VELOCITY_VARIANCE: {
      X: 0.01,
      Y: 0.01
    },
    GRAVITY: {
      X: 0,
      Y: 0
    },
    REFL: {
      X: -1,
      Y: 1
    },
    POS_VARIANCE: {
      X: 25,
      Y: 25
    },
    DURATION: 1000,
    PARTICLES: {
      SPEED: 0.1,
      SPEED_VARIANCE: 0.05,
      SPEED_DECAY: {
        X: 0.9,
        Y: 0.9
      },
      LIFE: 250,
      LIFE_VARIANCE: 250,
      SIZE: 50,
      SIZE_VARIANCE: 50,
      END_SIZE: 100,
      END_SIZE_VARIANCE: 10,
      DODGE_BURN_CHANCE: 0.25,
      COMPOSITE_LIGHTER_CHANCE: 0.5,
      RADIAL_SHARPNESS: 0,
      RADIAL_SHARPNESS_VARIANCE: 10
    }
  };

  Inkblot = (function() {
    function Inkblot(ctx) {
      this.ctx = ctx;
      this.emitters = [];
      this.colors = [];
      this.cfg = emitter_config;
    }

    Inkblot.prototype.init = function() {
      var emitter_cfg1, emitter_cfg2, i, no_refl, part1, part2, random_color, random_color_trans, random_pos, random_pos_refl, random_velocity, rng1, rng2, seed, x_refl, _i, _ref, _results;
      if (this.animation_rid != null) {
        cancelAnimationFrame(this.animation_rid);
      }
      this.some_alive = true;
      this.animation_rid = void 0;
      this.num_blots = Math.floor(Math.random() * 5);
      this.emitters = [];
      this.colors = [];
      this.current_time = void 0;
      no_refl = {
        X: 1,
        Y: 1
      };
      x_refl = {
        X: -1,
        Y: 1
      };
      _results = [];
      for (i = _i = 0, _ref = this.num_blots; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        random_pos = {
          x: Math.random() * this.ctx.canvas.width / 2,
          y: Math.random() * this.ctx.canvas.height
        };
        random_pos_refl = {
          x: this.ctx.canvas.width - random_pos.x,
          y: random_pos.y
        };
        random_velocity = {
          X: this.cfg.VELOCITY.X + this.cfg.VELOCITY_VARIANCE.X * signed_random(),
          Y: this.cfg.VELOCITY.Y + this.cfg.VELOCITY_VARIANCE.Y * signed_random()
        };
        seed = Date.now();
        rng1 = new Math.seedrandom(seed);
        rng2 = new Math.seedrandom(seed);
        random_color = {
          r: Math.floor(Math.random() * 255),
          g: Math.floor(Math.random() * 255),
          b: Math.floor(Math.random() * 255),
          a: 0.1
        };
        random_color_trans = {
          r: random_color.r,
          g: random_color.g,
          b: random_color.b,
          a: 0
        };
        this.colors.push([random_color.r, random_color.g, random_color.b, 255]);
        emitter_cfg1 = shallow_copy(this.cfg);
        emitter_cfg1.REFL = no_refl;
        emitter_cfg1.VELOCITY = shallow_copy(random_velocity);
        emitter_cfg1.PARTICLES.COLOR = random_color;
        emitter_cfg1.PARTICLES.COLOR_TRANS = random_color_trans;
        emitter_cfg2 = shallow_copy(this.cfg);
        emitter_cfg2.REFL = x_refl;
        emitter_cfg2.VELOCITY = shallow_copy(random_velocity);
        emitter_cfg2.PARTICLES.COLOR = random_color;
        emitter_cfg2.PARTICLES.COLOR_TRANS = random_color_trans;
        part1 = new ParticleEmitter(random_pos, rng1, this.ctx, emitter_cfg1);
        part2 = new ParticleEmitter(random_pos_refl, rng2, this.ctx, emitter_cfg2);
        this.emitters.push(part1);
        _results.push(this.emitters.push(part2));
      }
      return _results;
    };

    Inkblot.prototype.update = function(time) {
      var dt;
      if (this.current_time == null) {
        this.current_time = time;
      }
      dt = time - this.current_time;
      this.current_time = time;
      return this.step(dt);
    };

    Inkblot.prototype.step = function(dt) {
      var e, _i, _len, _ref, _results;
      this.some_alive = void 0;
      _ref = this.emitters;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        e = _ref[_i];
        if (e.some_alive) {
          e.step(dt);
        }
        if (e.some_alive) {
          _results.push(this.some_alive != null ? this.some_alive : this.some_alive = true);
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };

    Inkblot.prototype.generate = function() {
      var draw_prompt, that;
      this.init();
      that = this;
      draw_prompt = function(time) {
        if (that.some_alive) {
          that.update(time);
          return requestAnimationFrame(draw_prompt);
        } else {
          return cancelAnimationFrame(that.animation_rid);
        }
      };
      this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
      return this.animation_rid = requestAnimationFrame(draw_prompt);
    };

    return Inkblot;

  })();

  shallow_copy = function(obj) {
    return JSON.parse(JSON.stringify(obj));
  };

  signed_random = function(rng) {
    if (rng == null) {
      rng = Math.random;
    }
    return rng() * 2 - 1;
  };

  deg_to_rad = function(deg) {
    return deg * Math.PI / 180;
  };

  rad_to_deg = function(rad) {
    return rad * 180 / Math.PI;
  };

  wrap_angle = function(angle) {
    while (angle < -Math.PI) {
      angle += Math.PI * 2;
    }
    while (angle > Math.PI) {
      angle -= Math.PI * 2;
    }
    return angle;
  };

  color_string_from_rgba_object = function(rgba) {
    return 'rgba(' + rgba.r + ', ' + rgba.g + ', ' + rgba.b + ', ' + rgba.a + ')';
  };

  window.Inkblot = Inkblot;

}).call(this);
