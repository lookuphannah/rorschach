class RemoveTitleFromDrawings < ActiveRecord::Migration
  def up
    remove_column :drawings, :title
      end

  def down
    add_column :drawings, :title, :string
  end
end
