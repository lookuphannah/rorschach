require 'tempfile'

module Utils

  def decode(uri)
    if uri.match(%r{^data:(.*?);(.*?),(.*)$})
      type = $1
      encoder = $2
      data = $3
      if encoder == "base64"
        return Base64.decode64(data)
      end
    end

    raise "Illegal format error: #{uri.inspect}"
  end

  def save_image_from_url (url)
    file = Tempfile.new(['remote_image', '.jpg'])
    file.binmode
    file.write(open(url).read)
    file
  end

  def save_image_data (data)
    decodeData = decode(data)
    file = Tempfile.new(['prompt', '.png'])
    file.binmode
    file.write(decodeData)
    file
  end

end
