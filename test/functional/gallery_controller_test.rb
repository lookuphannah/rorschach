require 'test_helper'

class GalleryControllerTest < ActionController::TestCase
  test "should get prompts" do
    get :prompts
    assert_response :success
  end

  test "should get drawings" do
    get :drawings
    assert_response :success
  end

end
