class Drawing < ActiveRecord::Base
  include Utils
  attr_accessor :drawingData
  attr_accessible :drawingData, :image, :user_id
  before_validation :save_drawingData

  belongs_to :prompt, :inverse_of => :drawings
  belongs_to :user, :inverse_of => :drawings

  mount_uploader :image, PromptUploader
  validates_presence_of :user
  validates_presence_of :prompt

  def save_drawingData
    self.image = save_image_data drawingData
  end

end
