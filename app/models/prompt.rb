class Prompt < ActiveRecord::Base
  include Utils
  attr_accessor :promptData
  attr_accessible :image, :promptData, :drawings_attributes, :colors
  before_validation :save_promptData

  has_many :drawings, :dependent => :destroy, :inverse_of => :prompt

  accepts_nested_attributes_for :drawings, :limit => 1

  mount_uploader :image, PromptUploader

  validates_format_of :colors, :with => /\A\[(\[(\d{1,3},){3}\d{1,3}\],){0,19}\[(\d{1,3},){3}\d{1,3}\]\]\z/


  def save_promptData
    self.image = save_image_data promptData
  end


end
