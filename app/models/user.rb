class User < ActiveRecord::Base
  attr_accessible :name, :uid, :provider
  has_many :drawings, :inverse_of => :user

  validates_uniqueness_of :uid, :scope => :name

  def self.from_omniauth(auth)
    where(auth.slice('provider', 'uid')).first || create_from_omniauth(auth)
  end

  def self.create_from_omniauth(auth)
    create! do |user|
      user.provider = auth['provider']
      user.uid = auth['uid']
      user.name = auth['info']['nickname'] || auth['info']['name']
    end
  end

end
