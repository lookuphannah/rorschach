class SessionsController < ApplicationController
  def create
    user = User.from_omniauth(env['omniauth.auth'])
    if user.is_banned?
      session[:user_id] = nil
      redirect_to root_url,
      :notice => 'You have been banned from this site. You can view drawings, but not contribute your own.'
    else
      session[:user_id] = user.id
      redirect_to env['omniauth.origin'] || root_url, notice: 'Signed in!'
    end

  end

  def destroy
    session[:user_id] = nil
    redirect_to env['omniauth.origin'] || root_url, notice: 'Signed in!'
  end
end
