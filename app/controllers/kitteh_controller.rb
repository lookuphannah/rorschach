class KittehController < ApplicationController
  include Utils
  def index
    jsonp =open('http://api.flickr.com/services/feeds/photos_public.gne?tags=[' +
                  %w[kittens kitten cat cats].sample +
                  ']&format=json').read
    json = jsonp[jsonp.index('(') + 1 .. jsonp.rindex(')') - 1]
    kitteh = JSON.parse(json)['items'].sample

    remote_image = save_image_from_url (kitteh['media']['m'].gsub('_m.', '_z.'))
    send_file remote_image, :type => 'image/jpeg', :disposition => 'inline'
  end

end
