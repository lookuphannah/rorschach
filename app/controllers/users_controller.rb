class UsersController < ApplicationController

  helper_method :can_ban

  def index
    # @users = User.where(:is_banned => false).page(params[:page]).per(10)
    @users = User.joins(:drawings).uniq.page(params[:page]).per(10)
    respond_to do |format|
      format.html # show.html.erb
      format.js
      format.json { render json: @users }
    end
  end

  def show
    if User.exists?(:name => params[:name])
      @user = User.find_by_name(params[:name])
    elsif User.exists?(params[:id])
      @user = User.find(params[:id])
    else
      redirect_to root_url, :notice => 'No such user.'
    end

    @drawings = @user.drawings.page(params[:page]).per(10)
    respond_to do |format|
      format.html # show.html.erb
      format.js
      format.json { render json: @user }
    end

  end

  def update
    @user = User.find(params[:id])
    case params[:admin_action]
      when 'ban'
        if can_ban(current_user, @user)
          ban(@user)
          redirect_to users_path(), :notice => @user.name + ' has been banned.'
        else
          redirect_to @user, :notice => 'You are not authorized to ban this user.'
        end
      when 'unban'
        if can_ban(current_user, @user)
          unban(@user)
          redirect_to @user, :notice => @user.name + ' has been unbanned.'
        else
          redirect_to @user, :notice => 'You are not authorized to ban this user.'
        end
      else
        redirect_to @user, :notice => 'Unrecognized admin_action.'
    end
  end

  private
    def ban(user)
      user.drawings.destroy_all
      user.is_banned = true
      user.is_admin = false
      user.save
    end

    def unban(user)
      return if not user.is_banned?

      user.is_banned = false
      user.save

    end

    def can_ban(admin, target)
      admin and admin.is_admin? and admin != target and not target.is_admin?
    end

end
