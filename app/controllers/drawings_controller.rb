class DrawingsController < ApplicationController
  before_filter :get_prompt, :get_user

  # GET /drawings
  # GET /drawings.json
  def index
    @drawings = @prompt.drawings
    respond_to do |format|
      format.html { redirect_to @prompt }# index.html.erb
      #format.html
      format.json { render json: @drawings }
    end
  end

  def all
    @drawings = Drawing.page(params[:page]).per(10)
  end

  # GET /drawings/1
  # GET /drawings/1.json
  def show
    @drawing = @prompt.drawings.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @drawing }
    end
  end

  # GET /drawings/new
  # GET /drawings/new.json
  def new
    @drawing = @prompt.drawings.build
    @drawing.user = @user
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @drawing }
    end
  end

  # GET /drawings/1/edit
  def edit
    @drawing = @prompt.drawings.find(params[:id])
  end

  # POST /drawings
  # POST /drawings.json
  def create
    @drawing = @prompt.drawings.build(params[:drawing])
    @drawing.user = @user

    respond_to do |format|
      if @drawing.save!
        #format.html { redirect_to @drawing, notice: 'drawing was successfully created.' }
        format.html { redirect_to @prompt, notice: 'Drawing was successfully created' }
        format.json { render json: @drawing, status: :created, location: @drawing }
      else
        format.html { render action: 'new' }
        format.json { render json: @drawing.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /drawings/1
  # PUT /drawings/1.json
  def update
    @drawing = @prompt.find(params[:id])

    respond_to do |format|
      if @drawing.update_attributes(params[:drawing])
        #format.html { redirect_to @drawing, notice: 'drawing was successfully updated.' }
        format.html { redirect_to @prompt, notice: 'Drawing was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @drawing.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /drawings/1
  # DELETE /drawings/1.json
  def destroy
    @drawing = @prompt.drawings.find(params[:id])
    @drawing.destroy unless not current_user or (current_user != @drawing.user and not current_user.is_admin)

    respond_to do |format|
      #format.html { redirect_to drawings_url }
      format.html { redirect_to @prompt, notice: 'Drawing was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    def get_prompt
      if params[:prompt_id]
        @prompt = Prompt.find(params[:prompt_id])
      end
    end

    def get_user
      @user = current_user
    end
end

