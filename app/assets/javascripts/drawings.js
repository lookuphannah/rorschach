// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

//= require 'gallery'


$(function()
{
    bindDrawingItems = function(selector)
    {
        selector.find('.toggleInkblot').click(function()
        {

            $(this).parents('.drawingCard')
                .children('.prompt').stop(true, false).fadeToggle(1000);
        });

        selector.map(function()
        {
            $(this).delay(50 * $(this).index()).fadeIn(1500);
        });

        selector.hover(function()
            {
                $(this).find('.slideIns').stop(true, true).fadeIn();
            },
            function()
            {
                $(this).find('.slideIns').stop(true, true).fadeOut();
            });
    };

    bindDrawingItems($('.drawingItem'));

    $('#drawingWrapper').click(function()
    {
        $(this).children('#prompt').stop(true, false).fadeToggle(1000);
    });


});