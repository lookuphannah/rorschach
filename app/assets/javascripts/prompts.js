// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(function()
{
    bindPromptItems = function(selector)
    {
        selector.find('.openFolder').click(function()
        {
            if($(this).parents('.promptItem').hasClass('clicked'))
            {
                return;
            }

            $('.folder').stop(true, true);
            $('.clicked').removeClass('clicked');

            $(this).parents('.promptItem').addClass('clicked');

            var rowLength = Math.floor($('.gallery').width() / $('.galleryItem').width());
            var rowEnd = $(this).parents('.promptItem').index() + rowLength -
                ($(this).parents('.promptItem').index() % rowLength) - 1;
            if(rowEnd > $('.galleryItem').length - 1)
            {
                rowEnd = $('.galleryItem').length - 1;
            }

            if($('.folder').length === 0 || $('.folder').index() !== rowEnd + 1)
            {
                if($('.folder').length === 0)
                {
                    $('<ul></ul>').insertAfter($('.galleryItem')
                        .eq(rowEnd))
                        .addClass('folder')
                        .css('height', 150 * (Math.ceil($('.clicked').data().drawings.length / rowLength +
                            ($('.clicked').data.hasMore ? 1 : 0))))
                        .slideDown({complete: function()
                        {
                            $('body').animate({scrollTop: $('.folder').offset().top - 20});
                            displayPromptDrawings();
                        }});
                }
                else
                {
                    $('.folder').slideUp(400, function(){
                        $(this).remove();
                        $('<ul></ul>').insertAfter($('.galleryItem')
                            .eq(rowEnd))
                            .addClass('folder')
                            .css('height', 150 * (Math.ceil($('.clicked').data().drawings.length / rowLength +
                            ($('.clicked').data.hasMore ? 1 : 0))))
                            .slideDown({complete: function()
                            {
                                $('body').animate({scrollTop: $('.folder').offset().top - 20});
                                displayPromptDrawings();
                            }});

                    });
                }
            }
            else
            {
                $('.folder').animate({height: 150 * (Math.ceil($('.clicked').data().drawings.length / rowLength +
                    ($('.clicked').data.hasMore ? 1 : 0)))}, 500, displayPromptDrawings);

            }
        });

        selector.hover(function()
        {
            $(this).find('.slideIns').stop(true, true).fadeIn();
        },
        function()
        {
            $(this).find('.slideIns').stop(true, true).fadeOut();
        });

        selector.map(function()
        {
            $(this).delay(50 * $(this).index()).fadeIn(1500);
        });

        var displayPromptDrawings = function()
        {
            var promptDrawings = $('.clicked').data().drawings;



            if($('.folder').children().length === 0)
            {
                render_drawings(promptDrawings);

            }
            else
            {
                $('.folder').children().fadeOut(
                {
                    complete: function()
                    {

                        $('.folder').children().remove();
                        render_drawings(promptDrawings);
                    }
                });
            }

            function render_drawings(drawings) {
                for (var i = 0; i < drawings.length; i++) {
                    $('<li><a href="/inkblots/' + drawings[i].prompt_id + '/drawings/' + drawings[i].id +
                        '"><img src="' + drawings[i].image.url + '"></a></li>')
                        .appendTo('.folder')
                        .delay($('.folder li').length * 50)
                        .fadeIn();
                }

                if($('.clicked').data().hasMore)
                {
                    $('<li><a href="/inkblots/' + drawings[0].prompt_id + '"><img src="assets/viewall.png"></a></li>')
                        .appendTo('.folder')
                        .delay($('.folder li').length * 50)
                        .fadeIn();
                }
            }
        };
    };

    bindPromptItems($('.promptItem'));

});