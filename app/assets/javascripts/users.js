// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/


$(function()
{
    $('.banhammer, .unbanpillow').css('margin-top', '-125px').hover(function()
    {
        $(this).stop(true, false).animate({'margin-top': 0}, 500);

    },
    function()
    {
        $(this).stop(true, false).animate({'margin-top': -125}, 500);

    });

    bindDrawingItems = function(selector)
    {
        selector.click(function()
        {
            $(this).children('.drawingCard').children('.prompt').stop(true, false).fadeToggle(1000)
        });

        selector.map(function()
        {
            $(this).delay(50 * $(this).index()).fadeIn(1500);
        });

        selector.children('.drawingCard')
            .hover(function()
            {
                $(this).children('.slideIn').stop(true, true).slideDown();
            },
            function()
            {
                $(this).children('.slideIn').stop(true, true).slideUp();
            });
    };

    bindDrawingItems($('.drawingItem'));

    $('#drawingWrapper').click(function()
    {
        $(this).children('#prompt').stop(true, false).fadeToggle(1000)
    });

    bindUserItems = function(selector)
    {
        selector.map(function()
        {
            $(this).delay(50 * $(this).index()).fadeIn(1500);

        });

        selector.children('.userCard')
            .hover(function()
            {
                $(this).children('.slideIn').stop(true, true).slideDown();
            },
            function()
            {
                $(this).children('.slideIn').stop(true, true).slideUp();
            });

    };

    bindUserItems($('.userItem'));

});