// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(function()
{
    (function()
    {
        $('#staticContentWrapper').prepend('<div id="contents"></div>');
        var contents = $('#contents');
        $('.s').each(function()
        {
            var sectionAnchor = $(this).find('a.sectionAnchor').attr('id');
            var sectionTitle =  $(this).find('h1.t').text();
            contents.append('<a href="#' + sectionAnchor  + '"><h3>' + sectionTitle + '</h3></a>');

            var questionsList = '';

            $(this).find('h2.q').each(function()
            {
                questionsList += '<li><a href="#' +
                    $(this).find('a.questionAnchor').attr('id') + '">' + $(this).text() + '</li>'

            });

            $('<ul></ul>').append($(questionsList)).appendTo(contents);


        });
    })();

});
