// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(function()
{
    var calculateGalleryWidth = function()
    {
        return Math.floor($(window).width() / $('.galleryItem').width()) * $('.galleryItem').width();
    };

    var reactToResize = function()
    {
        $('.folder').stop(true, true).remove();
        $('.clicked').removeClass('clicked');
        $('.gallery').width(calculateGalleryWidth());
    };

    $('.gallery').width(calculateGalleryWidth());

    if(!window.isMobile.any())
    {
        $(window).resize(reactToResize);
    }

    $('#loadmore').click(function()
    {
        url = $('.pagination .next a').attr('href');
        $('.pagination, #loadmore').text('Loading...');
        $.getScript(url);
    });


});