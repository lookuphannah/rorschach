require 'bundler/capistrano'

set :application, 'rorschach'
set :user, 'shoja'
set :applicationdir, "/home/#{user}/dev/rorschdraw.com"
set :domain, 'rorschdraw.com:52280'

set :repository,  "ssh://#{user}@162.220.240.136:52280/home/#{user}/git/#{application}"
ssh_options[:forward_agent] = true
default_run_options[:pty] = true


set :scm, :git
set :branch, 'master'
set :got_shallow_clone, 1
set :scm_verbose, true

role :web, domain                          # Your HTTP server, Apache/etc
role :app, domain                          # This may be the same as your `Web` server
role :db,  domain, :primary => true        # This is where Rails migrations will run

set :deploy_to, applicationdir
set :deploy_via, :remote_cache

set :use_sudo, false
set :normalize_asset_timestamps, false

set :rvm_ruby_string, '1.9.3@rorschach'
set :rvm_autolibs_flag, 'read-only'        # more info: rvm help autolibs

before 'deploy:setup', 'rvm:install_rvm'   # install RVM
before 'deploy:setup', 'rvm:install_ruby'  # install Ruby and create gemset, OR:

after 'deploy:update_code', 'deploy_rorschdraw:symlink'
after 'deploy:update', 'deploy:cleanup'

load 'deploy/assets'

namespace :deploy_rorschdraw do
  desc 'Symlink'
  task :symlink do
    run "rm -rf #{release_path}/public/uploads"
    run "mkdir -p #{shared_path}/public/uploads"
    run "ln -nfs #{shared_path}/public/uploads #{release_path}/public/uploads"
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end
end

namespace :deploy do
  task :restart do
    run "touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end

require 'rvm/capistrano'