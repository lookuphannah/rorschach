Rorschach::Application.routes.draw do


  get 'info', to: 'static#info'
  get 'user/:name', to: 'users#show'

  match 'auth/:provider/callback', to: 'sessions#create'
  match 'signout', to: 'sessions#destroy', as: 'signout'
  match 'auth/failure', to: redirect('/')
  match 'drawings', to: 'drawings#all', as: 'drawings'
  match 'kitteh', to: 'kitteh#index', as: 'kitteh'


  resources :users, :only => [:index, :show, :update]

  # resources :prompts, :except => [:edit, :update] do
  resources :inkblots, :except => [:edit, :update], :controller => 'prompts', :as => 'prompts' do
    resources :drawings, :except => [:edit, :update]
  end

  root :to => 'prompts#new'

end
